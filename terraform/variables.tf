

variable "repository_branch" {
  description = "Repository branch to connect to"
  default     = "main"
}

variable "env" {
  description = "Depolyment environment"
  default     = "dev"
}
variable "project_name" {
  description = "Project name"
  default     = "aws-sagemaker-terraform-cicd"
}
variable "project_id" {
  description = "Project ID"
  default     = "10072022"
}
variable "region" {
  description = "AWS region"
  default     = "us-west-2"
}

variable "repository_owner" {
  description = "GitHub repository owner"
  default     = "aboroufar"
}

variable "build_repository_name" {
  description = "GitHub repository name"
  default     = "modelbuild-pipeline"
}

variable "deploy_repository_name" {
  description = "GitHub repository name"
  default     = "modeldeploy-pipeline"
}

variable "artifacts_bucket_name" {
  description = "S3 Bucket for storing artifacts"
  default     = "mlop-bucket"
}

variable "github_token" {
  description = "GitHub token"
  default     = "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"
}
variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}